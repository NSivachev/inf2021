

def q_sort(col):
    if len(col) <= 1:
        return col
    elem = col[len(col)//2]
    b_col = []
    s_col = []

    for item in col:

        if item[1] > elem[1]:
            b_col.append(item)

        elif item[1] < elem[1]:
            s_col.append(item)

        elif item[0] < elem[0]:
            b_col.append(item)

        elif item[0] > elem[0]:
            s_col.append(item)

    return q_sort(b_col) + [elem, ] + q_sort(s_col)


def main(parties, votes):

    party_votes = {}

    for party in parties:
        party_votes[party] = 0

    for vote in votes:
        party_votes[vote] += 1

    party_votes = list((party_votes.items()))
    party_votes = q_sort(party_votes)
    for tupl in party_votes:
        print(tupl[0])


if __name__ == '__main__':
    PARTIES_AMOUNT = 3
    VOTES_AMOUNT = 5
    parties = []
    votes = []

    print('PARTIES: ')
    for _ in range(PARTIES_AMOUNT):
        parties.append(input())

    print('VOTES: ')
    for _ in range(VOTES_AMOUNT):
        votes.append(input())

    main(parties, votes)

