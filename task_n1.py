

def main(amount_p, amount_st):
    stations = [0] * amount_st

    for _ in range(amount_p):
        in_data = input().split()[2:]
        stations_inp = list(map(int, in_data))
        stations[stations_inp[0]-1] += 1
        stations[stations_inp[1] - 1] -= 1

    for num in range(1, len(stations)):
        stations[num] += stations[num-1]

    max_st = max(stations)

    for num in range(len(stations)):
        if stations[num] == max_st:
            print(f'{num+1} - {num+2}')


if __name__ == '__main__':
    AMOUNT_ST = int(input())
    AMOUNT_P = 3
    main(AMOUNT_P, AMOUNT_ST)
